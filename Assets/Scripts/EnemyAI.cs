﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour, ICharControl
{


    // точки, между которыми бот будет двигаться, в ожидании игрока
    private char selectedWaypoint = 'z';

    public Transform waypointA;
    public Transform waypointB;
    public LayerMask targetLayer;

    public float waitTime;
    public float attackCD;

    public float walkSpeed = 3.0f;
    public float jumpSpeed = 5.0f;

    private Vector3 direction;
    private Vector3 startPosition;
    private Vector3 LastTargetLocation;

    private float curDist;
    private Vector3 spawnPoint;
    private bool isTarget = false;

    private bool escaped = false;

    private float timestamp = 0;
    private float waitTimeStamp = 0;

    private Enemy enemy;

    // Use this for initialization

    void Awake()
    {
        spawnPoint = transform.position;
        enemy = GetComponent<Enemy>();

    }
    void Start()
    {

    }



    Vector3 SetDirection(float xPos)
    {
        return new Vector3(xPos, transform.position.y, transform.position.z) - transform.position;
    }


    void Patrol() // зацикленное движение от А к В и обратно
    {
        float a = Vector3.Distance(transform.position, waypointA.position);
        float b = Vector3.Distance(transform.position, waypointB.position);

        if (Mathf.Abs(a) < 3 && selectedWaypoint == 'a')
        {
            if (!Wait())
            {
                direction = SetDirection(waypointB.position.x);
                selectedWaypoint = 'b';
            }
        }
        else if (Mathf.Abs(b) < 3 && selectedWaypoint == 'b')
        {
            if (!Wait())
            {
                direction = SetDirection(waypointA.position.x);
                selectedWaypoint = 'a';
            }
        }
        else if (direction.normalized.x == 0)
        {
            direction = SetDirection(waypointA.position.x);
            selectedWaypoint = 'a';
        }
        //
    }


    void Pursuit() // преследование игрока
    {
        direction = SetDirection(enemy.target.transform.position.x);
        LastTargetLocation = enemy.target.transform.position;
    }
    void PursuitToLastLocation()
    {
        if (!escaped)
        {
            direction = SetDirection(LastTargetLocation.x);
            escaped = true;
        }
        if (Mathf.Abs(Vector3.Distance(transform.position, LastTargetLocation)) < 3)
        {
            Debug.Log("KUKU");
            if (!Wait())
            {
                isTarget = false;
                escaped = false;
            }
        }
    }

    bool Wait()
    {
        if (waitTimeStamp > waitTime)
        {
            waitTimeStamp = 0;
            return false;
        }
        waitTimeStamp += Time.deltaTime;

        direction = Vector3.zero;

        return true;
    }

    bool SearchPlayer() // поиск игрока на пути следования
    {
        Vector3 dir = (transform.lossyScale.x) * transform.right * enemy.searchDistance;

        Debug.DrawRay(transform.position, dir, Color.blue);

        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, enemy.searchDistance, targetLayer);

        if (hit.collider && hit.transform.tag == "Player")
        {
            enemy.anim.SetBool("pursuit", true);
            return true;
        }
        enemy.anim.SetBool("pursuit", false);
        return false;
    }

    public void attack()
    {
        //print("FIIIIGHT");
        Fight2D.Action(enemy.damager.position, enemy.damagerRadius, targetLayer, enemy.Damage, false);
    }





    public void Update()
    {
        timestamp += Time.deltaTime;



        {

            if (!isTarget)
            {

                if (waypointA && waypointB)
                {
                    Patrol();
                    // Debug.Log("IM BACK!!!" + gameObject.name);
                }
                if (enemy.target)
                    if (SearchPlayer())
                        isTarget = true;
            }
            else
            {
                if (SearchPlayer())
                    Pursuit();
                else
                    PursuitToLastLocation();
            }

            // print(direction.normalized.x * walkSpeed * Time.deltaTime);

            enemy.Move(new Vector3(direction.normalized.x * walkSpeed * Time.deltaTime, 0, 0));
            enemy.anim.SetFloat("speed", Mathf.Abs(direction.normalized.x));
        }

        if (enemy.target)
        {
            float targetDis = Vector3.Distance(enemy.target.transform.position, enemy.damager.transform.position);
            if (targetDis < enemy.damagerRadius)
            {
                if (timestamp > attackCD)
                {
                    timestamp = 0;
                    enemy.anim.SetTrigger("attack");
                }
            }
        }

        if (direction.normalized.x > 0 && !enemy.isFacingRight)
            enemy.Flip();
        else if (direction.normalized.x < 0 && enemy.isFacingRight)
            enemy.Flip();


    }


    public void GenerateWaypoints(GameObject point) // вспомогательная функция для создания вейпоинтов
    {
        if (!waypointA && !waypointB)
        {
            GameObject obj = new GameObject(gameObject.name + "_Waypoints");
            obj.transform.position = transform.position;

            GameObject clone = Instantiate(point) as GameObject;
            clone.transform.parent = obj.transform;
            clone.transform.localPosition = new Vector2(3, 0);
            clone.name = "Point_A";
            waypointA = clone.transform;

            clone = Instantiate(point) as GameObject;
            clone.transform.parent = obj.transform;
            clone.transform.localPosition = new Vector2(-3, 0);
            clone.name = "Point_B";
            waypointB = clone.transform;
        }
    }


}
