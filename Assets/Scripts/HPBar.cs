﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HPBar : MonoBehaviour {

	public GameObject Owner=null;
    public bool isStaticPosition=false;
    private RectTransform hpbar;
    private RectTransform canvas;


    protected Character ownerChar;


    protected Slider slider;
    public float followSmooth;

    void Awake()
    {
        hpbar = GetComponent<RectTransform>();
        slider = hpbar.GetComponent<Slider>();
         

    }
	// Use this for initialization
	void Start () {
        if(isStaticPosition)
            canvas = GameObject.Find("Canvas").GetComponent<RectTransform>();
        else
        canvas = GameObject.Find("EnemyHP").GetComponent<RectTransform>();
        hpbar.SetParent(canvas);

     

        ownerChar = Owner.GetComponent<Character>();

        slider.maxValue = ownerChar.HP;
        slider.value = ownerChar.HP;

        if(isStaticPosition)
        hpbar.anchoredPosition =new Vector2(0,50);

    }
	
	// Update is called once per frame
	public virtual void Update () {
        if(!isStaticPosition)
		PositionUpdate ();
		ScaleUpdate ();
		DestroyCheck ();

	}
	public virtual void ScaleUpdate(){
        if (Owner)
        {
                slider.value = ownerChar.HP;
        }
	}

	public void PositionUpdate()
	{
        if (Owner)
        {
            Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main,Owner.transform.position);
            hpbar.anchoredPosition = screenPoint - canvas.sizeDelta / 2f;
        }

	}

     public static Vector3 GetScreenPosition(Transform transform,Canvas canvas,Camera cam)
      {
          Vector3 pos;
          float width = canvas.GetComponent<RectTransform>().sizeDelta.x;
          float height = canvas.GetComponent<RectTransform>().sizeDelta.y;
          float x = Camera.main.WorldToScreenPoint(transform.position).x / Screen.width;
          float y = Camera.main.WorldToScreenPoint(transform.position).y / Screen.height;
          pos = new Vector3(width * x - width / 2, y * height - height / 2);    
          return pos;    
      }
	public void DestroyCheck()
	{
		if (!Owner)
			Destroy (gameObject);
	}
}
