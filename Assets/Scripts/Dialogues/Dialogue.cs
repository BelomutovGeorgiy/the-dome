﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class Dialogue : MonoBehaviour {

    public string filename;
    private TextAsset data;

    public int partNumber;
    public int StartFromPart;

    private bool isEnd = true;
    private bool DialogueStarted = false;

    private bool isNext = true;

    private bool askMode = false;
  //  private List<string> answersUndecoded=new List<string>();

    protected Inventory inventory;

    public GameObject dialoguePanel;
    private DialoguePanel panel;

    void Awake()
    {
        inventory = GameObject.Find("Person").GetComponent<Inventory>();
        data = Resources.Load("Dialogues/" + filename) as TextAsset;
        panel = dialoguePanel.GetComponent<DialoguePanel>();
        panel.Hide();
       // Parse();
    }

/*    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            
        }
    }*/

    public void Continue()
    {
        isNext = true;
        panel.ClearButtons();
    }
    public void ContinueFromPart(int part)
    {
        isNext = true;
        panel.ClearButtons();
        StartFromPart = part;
        DialogueStarted=false;
    }


    public void BeginParse()
    {
        DialogueStarted = false;
        partNumber = -1;
        StartCoroutine(Parse());
    }
    public void StopParse()
    {
        DialogueStarted = false;
        StopAllCoroutines();
        panel.Hide();
        panel.ClearButtons();
        isNext = true;
    }
    public IEnumerator Parse()
    {
       
        string[] dataArray =data.text.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        isEnd = false;
        for (int i = 0; (i < dataArray.Length ) &&(!isEnd); i++)
        {  
            ParseNextLine(dataArray[i]);
            if (DialogueStarted)
          //  yield return new WaitForSeconds(1);
            yield return new WaitUntil(() =>isNext);
        }
        DialogueStarted = false;
        StopAllCoroutines();
        panel.Hide();
        isNext = true;
        yield break;
    }
    void ParseNextLine(string line)
    {
        Debug.Log(line);
        string[] words = line.Split('#');
        if ((!DialogueStarted&&(partNumber == StartFromPart || words[0] == "Part"))||DialogueStarted)
        {
            if (!DialogueStarted && (partNumber == StartFromPart))
                DialogueStarted = true;
            if (!askMode)
            {
                switch (words[0])
                {
                    case "Name":
                        panel.UpdateTitle(words[1]);
                        break;
                    case "Part":
                        partNumber = int.Parse(words[1]);
                        break;
                    case "Say":
                        Say(words[1]);
                        break;
                    case "Give":
                        if (words.Length <= 2)
                            Give(int.Parse(words[1]));
                        else
                            if (words[2] == "Condition")
                        {
                            switch (words[3])
                            {
                                case "NotHas":
                                    if(inventory.FindItemById(int.Parse(words[5]))!=null)
                                        Give(int.Parse(words[1]));
                                    break;
                                default:
                                    break;
                                
                            }
                        }
                        break;
                    case "StartFrom":
                        StartFromPart = int.Parse(words[1]);
                        break;
                    case "Ask":
                        askMode = true;
                        AddAsk(words[1]);
                        break;
                    case "End":
                        //Debug.Log("End Next is " + StartFromPart);
                        isEnd = true;
                        partNumber = -1;
                        break;
                    default:
                        Debug.Log("Unimplemented Command");
                        break;
                }
            }
            else
            {
                switch (words[0])
                {
                    case "Answer":
                        panel.AddButton(words[2], int.Parse(words[1]));
                        panel.Buttons[panel.Buttons.Count-1].GetComponent<Button>().onClick.AddListener(()=>ContinueFromPart(int.Parse(words[4])));
                        break;
                    case "AskEnd":
                        isNext = false;
                        askMode = false;
                        break;
                }

            }
        }
    }
    
    void Say(string word)
    {
        isNext = false;
        panel.UpdateAsk(word);
        panel.SetContinueButton();
        panel.Buttons[0].GetComponent<Button>().onClick.AddListener(Continue);
        panel.Show();

       //Debug.Log(word);
    }
    void AddAsk(string ask)
    {
        panel.UpdateAsk(ask);
        panel.Show();
    }
    void Give(int ID)
    {
        isNext = false;
        panel.UpdateAsk("You got: "+ Constants.Manager.GetComponent<ItemsBase>().FindItemByID(ID).Title);
        panel.SetContinueButton();
        panel.Buttons[0].GetComponent<Button>().onClick.AddListener(Continue);
        panel.Show();

       // Debug.Log("Get: " + Constants.Manager.GetComponent<ItemsBase>().FindItemByID(ID).Title);
        inventory.AddItem(ID);
    }
}
