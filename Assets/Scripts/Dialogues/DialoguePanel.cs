﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DialoguePanel : MonoBehaviour
{
    public string Text
    {
        get { return (AskText.GetComponent<Text>().text); }
    }
    public GameObject ButtonPrefab;

    private GameObject AnswersPanel;
    public GameObject AskText;
    public GameObject Title;

    private CanvasGroup cg;

    public List<GameObject> Buttons = new List<GameObject>();

    void Awake()
    {
        AnswersPanel = this.transform.FindChild("AnswersPanel").gameObject;
        cg = GetComponent<CanvasGroup>();
        //UpdatePanel("NPC-NAME", "can you help me?", "yes", "no");

    }

    public void AddButton(string answer, int id)
    {
      /*  foreach (GameObject go in Buttons)
            Destroy(go);
        Buttons.Clear();*/
       /* for (int i = 0; i < answers.Length; i++)
        {*/
            Buttons.Add(Instantiate(ButtonPrefab));
            Buttons[Buttons.Count-1].transform.SetParent(AnswersPanel.transform);
            Buttons[Buttons.Count-1].transform.localScale = new Vector3(1f, 1f, 1f);
            Buttons[Buttons.Count-1].GetComponent<AnswerButton>().id = id;
            Buttons[Buttons.Count-1].GetComponent<AnswerButton>().SetText(answer);
       // }//
    }
    public void SetContinueButton()
    {
       AddButton("Click to Continue",0);
    }
    public void ClearButtons()
    {
        foreach (GameObject go in Buttons)
            Destroy(go);
        Buttons.Clear();
    }
    public void UpdateAsk(string text)
    {
        AskText.GetComponent<Text>().text = text;
    }
    public void UpdateTitle(string title)
    {
        Title.GetComponent<Text>().text = title;
    }

    /*public void UpdatePanel(string title, string text, params string[] answers)
    {
        UpdateAnswers(answers);
        UpdateAsk(text);
        UpdateTitle(title);
    }*/

    public void Show()
    {
        cg.alpha = 1;
    }
    public void Hide()
    {
        ClearButtons();
        cg.alpha = 0;
    }
}
