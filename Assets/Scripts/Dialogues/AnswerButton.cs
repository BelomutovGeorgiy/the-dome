﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AnswerButton : MonoBehaviour {

    public int id;
    public GameObject Text;

    void Awake()
    {
        Text = this.transform.FindChild("Text").gameObject;
    }
   public void SetText(string value)
    {
        Text.GetComponent<Text>().text = value;
    }
    void SetPurpose()
    {
    }
}
