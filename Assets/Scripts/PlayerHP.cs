﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHP : MonoBehaviour {

    public Slider slider;
    // Use this for initialization
    public GameObject Owner = null;
    private Player player = null;

    public void Awake()
    {
        player = Owner.GetComponent<Player>();
        slider.maxValue = player.HPMax;
        slider.value = player.HPMax;
    }

    public void Update()
    {
        slider.value = player.HP;
    }

}
