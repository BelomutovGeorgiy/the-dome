using UnityEngine;
using System.Collections;
using System;
using UnityStandardAssets._2D;
using UnityEngine.UI;

public class Player : Character
{

    [HideInInspector]
    public bool staminaLock=false;


    //public float accel;
    public Transform damager;
    public Transform WeaponHandler;
    public Transform WeaponRightHandler;
    public Transform GunHandler;
    public Transform IKParent;
    public Transform Vision;
    public float damagerRadius;
    public float PickRadius;

    public LayerMask whatIsItems;


    public Transform groundCheck;
    //public LayerMask whatIsGround;
    public LayerMask WhoAreEnemies;
    public float groundCheckRadius = 0.1f;

    public WeaponData weapon;
    public WeaponData gun;
    // Use this for initialization


    private bool ground;
    // Update is called once per frame

    public void FixedUpdate()
    {
        staminaRegen();
    }


    public void staminaRegen()
    {
        if (Stamina < StaminaMax)
            Stamina+=StaminaRegenSpeed*10;
        if (Stamina <=0 && !staminaLock)
                staminaLock = true;
        if(Stamina>StaminaMin && staminaLock)
            staminaLock = false;
    }

    public void Interaction()
    {
        Collider2D coll = Physics2D.OverlapCircle(Vision.position, PickRadius, (1<<11));
        if (coll)
        {
            if (coll.GetComponent<ComponentItemLocker>())
            {
             if(coll.GetComponent<ComponentItemLocker>().AccessRequest(this.gameObject))
                {
                    if (coll.GetComponent<VehicleController>())
                        TakeVehicleControl(coll.gameObject);
                    if (coll.GetComponent<Swithable>())
                        coll.GetComponent<Swithable>().Switch();
                }
            }
            else
            {
                if (coll.GetComponent<VehicleController>())
                    TakeVehicleControl(coll.gameObject);
                if (coll.GetComponent<Swithable>())
                    coll.GetComponent<Swithable>().Switch();
                if (coll.GetComponent<ItemObject>())
                    PickUpWeapon(coll.gameObject);
                /* if (coll.GetComponent<FireWeapon>())
                     coll.GetComponent<FireWeapon>().Fire();*/
                if (coll.GetComponent<Dialogue>())
                    coll.GetComponent<Dialogue>().BeginParse();
            }
        }

    }

    public void TakeVehicleControl(GameObject Vehicle)
    {
        Camera.main.GetComponent<SmoothCamera2D>().target = Vehicle.transform;
        Camera.main.GetComponent<SmoothCamera2D>().dampTime /= 2;
        Camera.main.orthographicSize *= 2;
        Vehicle.GetComponent<VehicleController>().enabled = true;
        Vehicle.GetComponent<VehicleController>().StoredPlayer = this.gameObject;
        gameObject.SetActive(false);
    }
    public void PickUpWeapon(GameObject Item)
    {
        GetComponent<Inventory>().AddItem(Item.GetComponent<ItemObject>().id);
        Destroy(Item);
    }

    public void DropItem(GameObject Item)
    {
        Destroy(Item);
        anim.SetBool("ColdWeapon",false);
        anim.SetBool("FireWeapon",false);
    }

    public void attack()
    {
       // print(LayerMask.NameToLayer("Enemy"));
        Fight2D.Action(damager.position, damagerRadius, WhoAreEnemies, Damage, false);
    }
    public void armedAttack()
    {
        Stamina -= 700;
        Fight2D.Action(weapon.weaponPrefab.transform.FindChild("damager").position, damagerRadius,WhoAreEnemies, (weapon.item as ColdWeapon).Damage, false);
    }
    public void Shot()
    {
      //  weapon.GetComponent<FireWeapon>().Fire();
    }



    public bool isOnGround()
    {
        return Physics2D.OverlapCircle(groundCheck.transform.position, groundCheckRadius, Constants.ground);
    }
    public bool isOnLadder()
    {
        return Physics2D.OverlapCircle(groundCheck.position,groundCheckRadius,Constants.ladder);
    }

}
