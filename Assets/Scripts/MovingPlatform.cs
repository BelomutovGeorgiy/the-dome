﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : Swithable
{
    public Transform[] Stops;
    public float MoveSpeed=1;
    public int CurrentStop = 0;

    private Vector3 currentVelocity = Vector3.zero;

    public void MoveTo(int StopIndex)
    {
        //TODO: move platform to this stop
    }

    public override void Switch()
    {
        if (CurrentStop < Stops.Length - 1)
            CurrentStop++;
        else
            CurrentStop=0;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.SmoothDamp(transform.position, Stops[CurrentStop].position,ref currentVelocity, MoveSpeed);
        //transform.position = Vector3.Lerp(transform.position,Stops[CurrentStop].position, Time.deltaTime * MoveSpeed);
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        coll.transform.parent = transform;
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        coll.transform.parent = null;
    }

    public override void SwitchOn()
    {
        CurrentStop=Stops.Length-1;
    }

    public override void SwitchOff()
    {
        CurrentStop = 0; ;
    }
}
