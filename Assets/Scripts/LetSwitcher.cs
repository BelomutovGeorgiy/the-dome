﻿using UnityEngine;
using System.Collections;
using System;

public class LetSwitcher : Swithable {
    public Transform movement;
    public float defaultSpeed = 3.5f;
    public Animator anim;
    public override void SwitchOff()
    {
        anim.SetBool("on",false);
        movement.GetComponent<SurfaceEffector2D>().speed = 0;
    }

    public override void SwitchOn()
    {
        anim.SetBool("on", true);
        movement.GetComponent<SurfaceEffector2D>().speed = defaultSpeed;
    }
}
