﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public GameObject equipmentPanel;
    public GameObject inventoryPanel;
    public GameObject hotPanel;

    private GameObject hotSlotPanel;
    private GameObject invslotPanel;
    private GameObject eqslotPanel;

    public GameObject inventorySlot;
    public GameObject inventoryItem;


    public int slotAmount = 20;

    public List<Item> items = new List<Item>();
    public List<GameObject> slots = new List<GameObject>();


    void Start()
    {
        hotSlotPanel = hotPanel.transform.FindChild("Hot Slot Panel").gameObject;
        invslotPanel = inventoryPanel.transform.FindChild("Slot Panel").gameObject;
        eqslotPanel = inventoryPanel.transform.FindChild("EquipmentPanel").gameObject;

        for (int i = 0; i < slotAmount; i++)
        {
            items.Add(new Item());
            slots.Add(Instantiate(inventorySlot));
            slots[i].transform.SetParent(invslotPanel.transform);
            slots[i].transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            slots[i].GetComponent<Slot>().id = i;
        }
        for (int i = 1; i < eqslotPanel.transform.childCount - 1; i++)
        {
            items.Add(new Item());
            // Debug.Log(eqslotPanel.transform.GetChild(i));
            slots.Add(eqslotPanel.transform.GetChild(i).gameObject);
            eqslotPanel.transform.GetChild(i).gameObject.GetComponent<Slot>().id = slots.IndexOf(eqslotPanel.transform.GetChild(i).gameObject);
        }
        for (int i =0; i < hotSlotPanel.transform.childCount; i++)
        {
            items.Add(new Item());
            // Debug.Log(eqslotPanel.transform.GetChild(i));
            slots.Add(hotSlotPanel.transform.GetChild(i).gameObject);
            hotSlotPanel.transform.GetChild(i).gameObject.GetComponent<Slot>().id = slots.IndexOf(eqslotPanel.transform.GetChild(i).gameObject);
        }
        AddItem(3);
        AddItem(4);
        AddItem(5);
        AddItem(6);
        AddItem(7);
        AddItem(15);
    }

    public void AddItem(int id)
    {
        Item item = Constants.Manager.GetComponent<ItemsBase>().FindItemByID(id);

        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].ID == -1)
            {
                items[i] = item;
                GameObject obj = Instantiate(inventoryItem);
                obj.GetComponent<Image>().sprite = items[i].Sprite;
                obj.GetComponent<ItemData>().item = item;
                obj.GetComponent<ItemData>().slot = i;
                obj.name = items[i].Title;
                obj.transform.SetParent(slots[i].transform);
                obj.transform.localPosition = Vector3.zero;
                obj.transform.localScale = new Vector3(1, 1, 1);
                break;
            }
        }
    }
    public Item FindItemById(int id)
    {
        foreach (Item i in items)
        {
            if (i.ID == id)
                return i;
        }
        return null;
    }
}
