﻿using UnityEngine;
using System.Collections;

public class Controllers : MonoBehaviour {

    public RuntimeAnimatorController PrimeController;
    public RuntimeAnimatorController ArmedController;
    public RuntimeAnimatorController GunnedController;
}
