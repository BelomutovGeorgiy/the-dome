﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityStandardAssets._2D;

public class VehicleController : MonoBehaviour
{

    public GameObject StoredPlayer;
    public bool isFacingRight = false;

    public List<GameObject> HoverEngines;
    public List<GameObject> FlightEngines;

    public float Accel;
    public float MaxThrottle;

    private float curThrottle = 0;

    public bool isFlightActive = false;


    private float hoverHeight = 2f;
    public float hoverForce = 10f;

    private Rigidbody2D vehicleRigidbody;
    // Use this for initialization
    void Awake()
    {
        vehicleRigidbody = GetComponent<Rigidbody2D>();
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            vehicleRigidbody.gravityScale = Mathf.Abs(vehicleRigidbody.gravityScale - 1);
            isFlightActive = !isFlightActive;
        }
        if (Input.GetButtonDown("Interaction") && !isFlightActive)
        {
            StoredPlayer.SetActive(true);
            StoredPlayer.transform.position = new Vector3(transform.position.x, transform.position.y, StoredPlayer.transform.position.z);

            Camera.main.GetComponent<SmoothCamera2D>().target = StoredPlayer.transform;
            Camera.main.GetComponent<SmoothCamera2D>().dampTime *= 2;
            Camera.main.orthographicSize /= 2;
            this.enabled = false;
        }
        //надо бы изучить LINQ потому что я ваще хз как оно работает... возвращает следующий енум, и первый если дошло до конца

        HoverControl();
    }
    public void HoverControl()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (curThrottle < MaxThrottle)
            {
                hoverHeight += Accel;
                curThrottle += Accel;
            }
        }
        if (Input.GetKey(KeyCode.LeftControl))
        {
            if (curThrottle > 0)
            {
                hoverHeight -= Accel;
                curThrottle -= Accel;
            }
        }


    }
    void FixedUpdate()
    {

        foreach (GameObject hoverEngine in HoverEngines)
        {

            Debug.DrawRay(hoverEngine.transform.position, -hoverEngine.transform.up * hoverHeight, Color.blue);
            RaycastHit2D hit = Physics2D.Raycast(hoverEngine.transform.position, -hoverEngine.transform.up, hoverHeight, ~(1 << 14));
            if (hit.collider)
            {
                float proportionalHeight = (hoverHeight * 10 - hit.distance) / hoverHeight * 10;
                Vector3 appliedHoverForce = Vector3.up * 2 * hoverForce;
                vehicleRigidbody.AddForce(appliedHoverForce);
            }
        }
        if (isFlightActive)
        {
            float h = Input.GetAxis("Horizontal");
            if (h > 0 && !isFacingRight)
                Flip();
            else if (h < 0 && isFacingRight)
                Flip();

            float verticalAxis = Input.GetAxis("Vertical");
            foreach (GameObject flightEngine in FlightEngines)
            {
                flightEngine.transform.GetChild(0).transform.localScale = Vector3.one * (curThrottle / MaxThrottle)* Mathf.Sign(-transform.localScale.x);
                Debug.DrawRay(flightEngine.transform.position, flightEngine.transform.right * hoverHeight, Color.magenta);
                //Vector3 appliedHoverForce = new Vector3((curThrottle - (curThrottle * Mathf.Abs(verticalAxis / 2)))*Mathf.Sign(transform.localScale.x), curThrottle * verticalAxis);
                Vector3 appliedHoverForce = new Vector3(curThrottle * hoverForce * Mathf.Sign(transform.localScale.x) / 2, verticalAxis * curThrottle * hoverForce / 2);
                vehicleRigidbody.AddForce(appliedHoverForce);
                // vehicleRigidbody.AddForce(appliedHoverForce);

            }
        }
        else
        {
            foreach (GameObject hoverEngine in HoverEngines)
            {
                hoverEngine.transform.GetChild(0).transform.localScale = Vector3.one * ((curThrottle * 0.3f / MaxThrottle) + 0.7f);
            }
            foreach (GameObject flightEngine in FlightEngines)
            {
                flightEngine.transform.GetChild(0).transform.localScale = Vector3.zero;
            }
        }
    }
    public void Flip()
    {
        //меняем направление движения персонажа
        isFacingRight = !isFacingRight;
        //получаем размеры персонажа
        Vector3 theScale = transform.localScale;
        //зеркально отражаем персонажа по оси Х
        theScale.x *= -1;
        //задаем новый размер персонажа, равный старому, но зеркально отраженный
        transform.localScale = theScale;
    }


}
