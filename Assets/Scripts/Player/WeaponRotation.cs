﻿using UnityEngine;
using System.Collections;

public class WeaponRotation : MonoBehaviour {


    public int rotationOffset=90;

    public int AngleLimitMax = 180;
    public int AngleLimitMin = 0;

    public float invert = 1;
//Update is called once per frame
	void Update () {

        invert = transform.root.localScale.x;

        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        difference.Normalize();

        float rotZ = Mathf.Atan2(difference.y, difference.x*invert) * Mathf.Rad2Deg;

        if (rotZ > AngleLimitMin && rotZ < AngleLimitMax)
            transform.rotation = Quaternion.Euler(0f, 0f, invert*rotZ + rotationOffset);

        /*else
        {
            if(Mathf.Abs(rotZ-90)<1)
            transform.parent.parent.parent.GetComponent<Player>().Flip();
            //AngleLimitMax*=-1;
            //AngleLimitMin*=-1;
        }*/
    }

}
