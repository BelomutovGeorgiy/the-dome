﻿using UnityEngine;
using System.Collections;

public class WeaponData {

    public Item item;
    public GameObject weaponPrefab;

    public WeaponData(ColdWeapon item, GameObject prefab)
    {
        this.item = item;
        this.weaponPrefab = prefab;
    }
    public WeaponData(HotWeapon item, GameObject prefab)
    {
        this.item = item;
        this.weaponPrefab = prefab;
    }
}
