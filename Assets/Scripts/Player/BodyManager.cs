﻿using UnityEngine;
using System.Collections;

public class BodyManager : MonoBehaviour
{

    public Transform HemletHandler;
    public Transform SpineBone;
    public Transform BodyArm_L_Bone;
    public Transform BodyArm_R_Bone;

    public Transform Leg_L_Bone;
    public Transform Leg_R_Bone;
    public Transform Leg_Low_L_Bone;
    public Transform Leg_Low_R_Bone;

    public Transform Arm_Bone_L;
    public Transform Arm_Bone_R;
    public Transform Arm_Low_Bone_L;
    public Transform Arm_Low_Bone_R;

    public Transform Foot_L_Bone;
    public Transform Foot_R_Bone;

    public Transform Fist_L_Bone;
    public Transform Fist_R_Bone;

    public Transform Arm_L_IK;
    public Transform Arm_R_IK;


    void Update()
    {
       /* if (Input.GetKeyDown(KeyCode.Q))
        {
           StartCoroutine( ChangeBoots("standard-armor"));
        }*/
    }


    public void ChangeHemlet(string setName)
    {
        if (HemletHandler.childCount == 1)
            Destroy(HemletHandler.GetChild(0).gameObject);


        GameObject newHemlet = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/Hemlet");

        if (newHemlet)
        {
            newHemlet = Instantiate(newHemlet) as GameObject;
            newHemlet.transform.SetParent(HemletHandler);
            newHemlet.transform.localPosition = Vector3.zero;
            newHemlet.transform.localRotation = HemletHandler.transform.localRotation;
            newHemlet.transform.localScale = HemletHandler.transform.localScale;
        }

    }
    public void ChangeBody(string setName)
    {

        Transform oldBody = SpineBone.FindChild("Body");
        Transform oldLeftArm = BodyArm_L_Bone.FindChild("BodyArm_L");
        Transform oldRightArm = BodyArm_R_Bone.FindChild("BodyArm_R");

        GameObject newBody = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/Body");
        GameObject newLeftArm = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/BodyArm_L");
        GameObject newRightArm = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/BodyArm_R");

        Change(oldBody.gameObject, newBody);
        Change(oldLeftArm.gameObject, newLeftArm);
        Change(oldRightArm.gameObject, newRightArm);

    }
    public void ChangeBoots(string setName)
    {

        Transform oldLeftBoot = Foot_L_Bone.FindChild("Foot_L");
        Transform oldRightBoot = Foot_R_Bone.FindChild("Foot_R");

        GameObject newLeftBoot = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/Foot_L");
        GameObject newRightBoot = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/Foot_R");

        Change(oldLeftBoot.gameObject,newLeftBoot);
        Change(oldRightBoot.gameObject, newRightBoot);

       StartCoroutine(RebindAnimator());
       
        
    }
    public void ChangeGloves(string setName)
    {

        Transform oldLeftFist= Fist_L_Bone.FindChild("Fist_L");
        Transform oldRightFist = Fist_R_Bone.FindChild("Fist_R");

        GameObject newLeftFist= Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/Fist_L");
        GameObject newRightFist = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/Fist_R");

        Change(oldLeftFist.gameObject,  newLeftFist  );
        Change(oldRightFist.gameObject, newRightFist);

    }
    public void ChangeLeggings(string setName)
    {
        Transform oldBodyBot = SpineBone.FindChild("BodyBot");
        Transform oldLeg_L = Leg_L_Bone.FindChild("Leg_L");
        Transform oldLeg_R = Leg_R_Bone.FindChild("Leg_R");
        Transform oldLeg_Low_L = Leg_Low_L_Bone.FindChild("Leg_Low_L");
        Transform oldLeg_Low_R = Leg_Low_R_Bone.FindChild("Leg_Low_R");

        GameObject newBodyBot = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/BodyBot");
        GameObject newLeg_L = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/Leg_L");
        GameObject newLeg_R = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/Leg_R");
        GameObject newLeg_Low_L = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/Leg_Low_L");
        GameObject newLeg_Low_R = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/Leg_Low_R");

        Change(oldBodyBot.gameObject, newBodyBot);
        Change(oldLeg_L.gameObject, newLeg_L);
        Change(oldLeg_R.gameObject, newLeg_R);
        Change(oldLeg_Low_L.gameObject, newLeg_Low_L);
        Change(oldLeg_Low_R.gameObject, newLeg_Low_R);

    }

    public void ChangeArms(string setName)
    {
        Transform oldArm_L = Arm_Bone_L.FindChild("Arm_L");
        Transform oldArm_R = Arm_Bone_R.FindChild("Arm_R");
        Transform oldArm_Low_L = Arm_Low_Bone_L.FindChild("Arm_Low_L");
        Transform oldArm_Low_R = Arm_Low_Bone_R.FindChild("Arm_Low_R");

        GameObject newArm_L = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/Arm_L");
        GameObject newArm_R = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/Arm_R");
        GameObject newArm_Low_L = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/Arm_Low_L");
        GameObject newArm_Low_R = Resources.Load<GameObject>("Prefabs/Armor/" + setName + "/Arm_Low_R");

        Change(oldArm_L.gameObject, newArm_L);
        Change(oldArm_R.gameObject, newArm_R);
        Change(oldArm_Low_L.gameObject, newArm_Low_L);
        Change(oldArm_Low_R.gameObject, newArm_Low_R);

    }


    public void Change(GameObject oldObj, GameObject newObj)
    {
        newObj = Instantiate(newObj) as GameObject;
        newObj.transform.SetParent(oldObj.transform.parent);

        newObj.transform.localPosition = oldObj.transform.localPosition;
        newObj.transform.localRotation = oldObj.transform.localRotation;
        newObj.transform.localScale = oldObj.transform.localScale;

        newObj.name = oldObj.name;
        Destroy(oldObj);

        newObj.name = oldObj.name;
    }


    //корутина-костыль, перезагружающая ониматор на следующем обновлении, использовать при смене шмоток которые почему-то ломают анимацию
    public IEnumerator RebindAnimator()
    {
        yield return new WaitForFixedUpdate();
        transform.parent.GetComponent<Animator>().Rebind();
        yield break;
    }
}
