﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComponentItemLocker : MonoBehaviour
{

    public bool isLock = true;
    public int KeyID = 0;


    private DialoguePanel panel;
    private Item key;
    // Use this for initialization
    void Start()
    {
        panel = GameObject.Find("Manager").GetComponent<Refery>().DialoguePanel.GetComponent<DialoguePanel>();
        if (isLock)//оптимизация
            key = GameObject.Find("Manager").GetComponent<ItemsBase>().FindItemByID(KeyID);
    }


    public bool AccessRequest(GameObject sender)
    {
        if (!isLock)
            return true;
        else
        if (sender.GetComponent<Inventory>())
        {
            if (sender.GetComponent<Inventory>().FindItemById(KeyID) == null)
            {
                //доступ запрещен
                panel.ClearButtons();
                panel.UpdateAsk(key.Name + " Is Required to use this");
                panel.SetContinueButton();
                panel.Buttons[0].GetComponent<Button>().onClick.AddListener(panel.Hide);
                panel.Show();

                return false;
            }
            else
            {
                //доступ получен
                panel.ClearButtons();
                panel.UpdateAsk("Used: " + key.Name);
                panel.SetContinueButton();
                panel.Buttons[0].GetComponent<Button>().onClick.AddListener(panel.Hide);
                panel.Show();

                return true;
            }
        }
        else
            return false;
    }
}
