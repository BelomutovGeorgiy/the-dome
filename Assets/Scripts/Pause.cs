﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class Pause : MonoBehaviour {

    private bool isPause=false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseSwitch();
        }
	}

    private void pauseSwitch()
    {
        if (isPause)
        {
            isPause = false;
            Time.timeScale = 1;
            Camera.main.GetComponent<Grayscale>().enabled = false;
        }
        else
        {
            isPause = true;
            Time.timeScale = 0;
            Camera.main.GetComponent<Grayscale>().enabled = true;
        }
    }
}
