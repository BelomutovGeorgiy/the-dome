﻿using UnityEngine;
using System.Collections;

public class Gate : MonoBehaviour
{
    public bool isOpen = false;
    public Animator anim;

    private Collider2D guest;
    public float duration = 5;

    private float time;
    private ComponentItemLocker objectLock=null;
    // Use this for initialization
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        objectLock = gameObject.GetComponent<ComponentItemLocker>();
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
    }


    void OnTriggerEnter2D(Collider2D coll)
    {
        if (!objectLock)
        {
            Open();
            guest = coll;
        }
        else
        {
            if(objectLock.AccessRequest(coll.gameObject))
            {
                Open();
                guest = coll;
            }
        }

    }
    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll == guest || time > duration)
        {
            guest = null;
            Close();
        }


    }


    void Open()
    {
        anim.SetBool("open", true);
        isOpen = true;
        time = 0;

    }
    void Close()
    {
        anim.SetBool("open", false);
        isOpen = false;
    }
}
