﻿using UnityEngine;
using System.Collections;

public abstract class Swithable : MonoBehaviour {

    public AnimationClip on;
    public AnimationClip off;

    public bool isOn = true;

    void Start()
    {
        if (on)
        {
            on.legacy = true;
            GetComponent<Animation>().AddClip(on, "on");
        }
        if (off)
        {
            off.legacy = true;
            GetComponent<Animation>().AddClip(off, "off");
        }
       
        
    }

    public virtual void Switch()
    {
        if (isOn)
        {
            isOn = false;
            SwitchOff();
        }
        else
        {
            isOn = true;
            SwitchOn();

        }
    }

    public abstract void SwitchOn();
    public abstract void SwitchOff();
}
