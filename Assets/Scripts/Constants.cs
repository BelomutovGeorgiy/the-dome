﻿using UnityEngine;
using System.Collections;


public class Constants: MonoBehaviour {
    public LayerMask Mground;
    public LayerMask Mitems ;
    public LayerMask Mladder;

  public static LayerMask ground;
  public static LayerMask items;
  public static LayerMask ladder;

    public static GameObject Manager;


    public static string _ITEMSBASEPATH = "/StreamingAssets/items.json";

    void Awake()
    {
        Manager = gameObject;

        ground = Mground;
        items = Mitems;
        ladder = Mladder;


    }
    
}
