﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour {

	public GameObject[] backgrounds;
    public GameObject player;
	private float[] parallaxScales;


    public float smoothing;

	private GameObject cam;
	private Vector3 previousCamPos;

	// Use this for initialization
	void Start () {
		cam=this.gameObject;
		previousCamPos = cam.transform.position;
		parallaxScales=new float[backgrounds.Length];

       
        for (int i=0; i<backgrounds.Length;i++) 
			parallaxScales[i]=(backgrounds[i].transform.position.z-player.transform.position.z)*-1;
    }
	
	// Update is called once per frame
	void LateUpdate () {
		for (int i=0; i<backgrounds.Length; i++) 
		{
            float parallax = (previousCamPos.x-cam.transform.position.x)*parallaxScales[i];
            float parallay = (previousCamPos.y - cam.transform.position.y) * parallaxScales[i];
            float backgroundTargetPosY = backgrounds[i].transform.position.y + parallay;
            float backgroundTargetPosX=backgrounds[i].transform.position.x+parallax;
            
                Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, backgroundTargetPosY, backgrounds[i].transform.position.z);

                backgrounds[i].transform.position = Vector3.Lerp(backgrounds[i].transform.position, backgroundTargetPos, smoothing * Time.deltaTime);
            
		}
        previousCamPos = cam.transform.position;
	}
}
