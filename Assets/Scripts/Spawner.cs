﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject[] go;
	public float delay;
	private float spawnStamp;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Spawn ();
	}
	void Spawn()
	{
		if(Time.time>spawnStamp)
		{
			spawnStamp=Time.time+delay;
			GameObject CurGo;
			CurGo=Instantiate(go[(int)(Random.value*2)],this.transform.position,this.transform.rotation) as GameObject;
            CurGo.transform.localScale = transform.localScale;
			CurGo.transform.parent=transform;
           
		}
	}
}
