﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class Slot : MonoBehaviour, IDropHandler
{
    public int id;
    protected Inventory inventory;
    public GameObject bodyManager;
    protected Player player;


    public virtual void OnDrop(PointerEventData eventData)
    {
        Debug.Log("DROP");
        ItemData droppedItem = eventData.pointerDrag.GetComponent<ItemData>();

        if (inventory.items[id].ID == -1)
        {
            if (inventory.slots[droppedItem.slot].GetComponent<EquipmentSlot>())
            {
                inventory.slots[droppedItem.slot].GetComponent<EquipmentSlot>().dropEquipment(droppedItem);
            }


            inventory.items[droppedItem.slot] = new Item();
            inventory.items[id] = droppedItem.item;
            droppedItem.slot = id;


        }
        else
        {
            Transform item = this.transform.GetChild(0);
            item.GetComponent<ItemData>().slot = droppedItem.slot;
            item.transform.SetParent(inventory.slots[droppedItem.slot].transform);
            item.transform.position = inventory.slots[droppedItem.slot].transform.position;

            if (inventory.slots[droppedItem.slot].GetComponent<EquipmentSlot>())
            {
                inventory.slots[droppedItem.slot].GetComponent<EquipmentSlot>().pickItem(item.GetComponent<ItemData>());
            }

            droppedItem.slot = id;
            droppedItem.transform.position = this.transform.position;

            inventory.items[droppedItem.slot] = item.GetComponent<ItemData>().item;
            inventory.items[id] = droppedItem.item;


        }
    }



    void Start()
    {
        inventory = GameObject.Find("Person").GetComponent<Inventory>();
        player = GameObject.Find("Person").GetComponent<Player>();
        bodyManager =player.transform.FindChild("Root").gameObject;
    }

}
