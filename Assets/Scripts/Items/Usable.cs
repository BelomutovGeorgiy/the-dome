﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Items
{
    class Usable : Item
    {
        
        public ArmorTypes Atype;
        public Usable(int id, string title, string description, string name,string usetype,bool isStackable, params float[] values) : base(id, ItemTypes.USABLE, title, description, name)
        {

        }
    }
}
