﻿using UnityEngine;
using System.Collections;

public class Armor : Item
{

    public float armor;
    public ArmorTypes Atype;
    public string setName;

    public Armor(int id, string title, string description, string name, float armor) : base(id, ItemTypes.ARMOR, title, description, name)
    {
        this.armor = armor;
        string[] nameParts=name.Split('-');

        setName = nameParts[0] + "-armor";

        switch (nameParts[nameParts.Length-1])
        {
            case "hemlet": Atype = ArmorTypes.HEMLET; break;
            case "body": Atype = ArmorTypes.BODY; break;
            case "legs": Atype = ArmorTypes.LEGS; break;
            default: Atype = ArmorTypes.ELSE; break;
        }
    }
}
