﻿using UnityEngine;
using System.Collections;

public enum ArmorTypes
{
    HEMLET,
    BODY,
    LEGS,
    ARMS,
    BOOTS,
    GLOVES,
    ELSE
}
