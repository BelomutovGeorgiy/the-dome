﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class ItemData : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler
{

    public Item item;
    private Inventory inventory;
    private DescriptionPanel descriptionPanel;
    public Transform originalParent;

    public int slot;

    private Color defaultColor;
    private Color highlightColor=new Color(1,0,0,1);

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (item != null)
        {
            originalParent = this.transform.parent;
            this.transform.SetParent(this.transform.parent);
            this.transform.position = eventData.position;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (item != null)
        {
            this.transform.position = eventData.position;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        this.transform.SetParent(inventory.slots[slot].transform);
        this.transform.position = inventory.slots[slot].transform.position;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    // Use this for initialization
    void Start()
    {
        inventory = GameObject.Find("Person").GetComponent<Inventory>();
        descriptionPanel = GameObject.Find("Description Panel").GetComponent<DescriptionPanel>();
        defaultColor = inventory.slots[slot].GetComponent<Image>().color;
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        inventory.slots[slot].GetComponent<Image>().color = highlightColor;
        descriptionPanel.Show(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        inventory.slots[slot].GetComponent<Image>().color = defaultColor;
        descriptionPanel.Hide();
    }
}
