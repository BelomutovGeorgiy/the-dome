﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class EquipmentSlot : Slot
{
    public ItemTypes type;

    public override void OnDrop(PointerEventData eventData)
    {

        ItemData droppedItem = eventData.pointerDrag.GetComponent<ItemData>();
        if ((droppedItem.item.Type == type)||
            (droppedItem.item.Type == ItemTypes.HOTWEAPON && type == ItemTypes.COLDWEAPON))//говнокод исключение, если в слот для холодного оружия кладу пушку (в дальнейшем нужно сделать общий тип для оружия и подтипы видов)
        {
            if (inventory.items[id].ID == -1)
            {

                inventory.items[droppedItem.slot] = new Item();
                inventory.items[id] = droppedItem.item;
                droppedItem.slot = id;

                pickItem(droppedItem);


            }
            else
            {
                Transform item = this.transform.GetChild(1);
                item.GetComponent<ItemData>().slot = droppedItem.slot;
                item.transform.SetParent(inventory.slots[droppedItem.slot].transform);
                item.transform.position = inventory.slots[droppedItem.slot].transform.position;

                droppedItem.slot = id;
                droppedItem.transform.position = this.transform.position;

                pickItem(droppedItem);


                inventory.items[droppedItem.slot] = item.GetComponent<ItemData>().item;
                inventory.items[id] = droppedItem.item;
            }
        }
    }
    public void pickItem(ItemData droppedItem)
    {
        switch (droppedItem.item.Type)
        {
            case ItemTypes.COLDWEAPON:
                dropEquipment(droppedItem);
                player.weapon = new WeaponData(droppedItem.item as ColdWeapon, (droppedItem.item as ColdWeapon).prefab);
                player.weapon.weaponPrefab = Instantiate(player.weapon.weaponPrefab, player.WeaponHandler.position, player.WeaponHandler.rotation) as GameObject;
                player.weapon.weaponPrefab.transform.parent = player.WeaponHandler;
                player.anim.SetBool("ColdWeapon", true);

                player.weapon.weaponPrefab.transform.localScale = new Vector3(1, 1, 1);
                break;
            case ItemTypes.HOTWEAPON:
                dropEquipment(droppedItem);
                player.gun = new WeaponData(droppedItem.item as HotWeapon, (droppedItem.item as HotWeapon).prefab);
                player.gun.weaponPrefab = Instantiate(player.gun.weaponPrefab, player.GunHandler.position, player.GunHandler.rotation) as GameObject;

                bodyManager.GetComponent<BodyManager>().Arm_L_IK.parent = player.gun.weaponPrefab.GetComponent<FireWeapon>().LeftIK;
                bodyManager.GetComponent<BodyManager>().Arm_R_IK.parent = player.gun.weaponPrefab.GetComponent<FireWeapon>().RightIK;

                bodyManager.GetComponent<BodyManager>().Arm_L_IK.position = player.gun.weaponPrefab.GetComponent<FireWeapon>().LeftIK.position;
                bodyManager.GetComponent<BodyManager>().Arm_R_IK.position = player.gun.weaponPrefab.GetComponent<FireWeapon>().RightIK.position;

                bodyManager.GetComponent<BodyManager>().Arm_L_IK.rotation = player.gun.weaponPrefab.GetComponent<FireWeapon>().LeftIK.rotation;
                bodyManager.GetComponent<BodyManager>().Arm_R_IK.rotation = player.gun.weaponPrefab.GetComponent<FireWeapon>().RightIK.rotation;



                player.gun.weaponPrefab.transform.parent = player.GunHandler;
                player.anim.runtimeAnimatorController = player.GetComponent<Controllers>().GunnedController;
                player.anim.Rebind();
                player.gun.weaponPrefab.transform.localScale = new Vector3(1, 1, 1);

                
                break;
            case ItemTypes.ARMOR:
                if (droppedItem.item is Armor)
                {
                    switch ((droppedItem.item as Armor).Atype)
                    {
                        case ArmorTypes.HEMLET:
                            bodyManager.GetComponent<BodyManager>().ChangeHemlet((droppedItem.item as Armor).setName);
                            break;
                        case ArmorTypes.LEGS:
                            bodyManager.GetComponent<BodyManager>().ChangeLeggings((droppedItem.item as Armor).setName);
                            bodyManager.GetComponent<BodyManager>().ChangeBoots((droppedItem.item as Armor).setName);
                            break;
                        case ArmorTypes.BODY:
                            foreach (Component c in bodyManager.GetComponents<Component>())
                                Debug.Log(c);
                            Debug.Log(bodyManager.GetComponent<BodyManager>());
                            bodyManager.GetComponent<BodyManager>().ChangeBody((droppedItem.item as Armor).setName);
                            bodyManager.GetComponent<BodyManager>().ChangeArms((droppedItem.item as Armor).setName);
                            break;
                    }
                }
                break;
            default:
                break;
        }
    }
    public void dropEquipment(ItemData droppedItem)
    {
        switch (droppedItem.item.Type)
        {
            /*  case ItemTypes.COLDWEAPON:
                  if (player.weapon != null)
                  {
                      player.DropItem(player.weapon.weaponPrefab);
                      player.weapon = null;
                  }
                  break;*/
            case ItemTypes.HOTWEAPON:
            case ItemTypes.COLDWEAPON:
                if (player.gun != null)
                {
                    bodyManager.GetComponent<BodyManager>().Arm_L_IK.parent = player.IKParent;
                    bodyManager.GetComponent<BodyManager>().Arm_R_IK.parent = player.IKParent;

                    player.DropItem(player.gun.weaponPrefab);
                    player.gun = null;

                    player.anim.runtimeAnimatorController = player.GetComponent<Controllers>().PrimeController;
                    player.anim.Rebind();
                }
                if (player.weapon != null)
                {
                    player.DropItem(player.weapon.weaponPrefab);
                    player.weapon = null;
                }
                break;
            case ItemTypes.ARMOR:
                {
                    if (droppedItem.item is Armor)
                    {
                        switch ((droppedItem.item as Armor).Atype)
                        {
                            case ArmorTypes.HEMLET:
                                bodyManager.GetComponent<BodyManager>().ChangeHemlet("naked-parts");
                                break;
                            case ArmorTypes.LEGS:
                                bodyManager.GetComponent<BodyManager>().ChangeLeggings("naked-parts");
                                bodyManager.GetComponent<BodyManager>().ChangeBoots("naked-parts");
                                break;
                            case ArmorTypes.BODY:
                                bodyManager.GetComponent<BodyManager>().ChangeBody("naked-parts");
                                bodyManager.GetComponent<BodyManager>().ChangeArms("naked-parts");
                                break;
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
}
