﻿using UnityEngine;
using System.Collections;

public enum ItemTypes {
    TEXT,
    AUDIO,
    COLDWEAPON,
    HOTWEAPON,
    ARMOR,
    SHIELD,
    USABLE
}
