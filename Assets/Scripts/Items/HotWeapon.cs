﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class HotWeapon : Item
{

    public float BulletDamage { get; set; }
    public float BulletSpeed { get; set; }
    public float ReloadSpeed { get; set; }

    public GameObject prefab;

    public HotWeapon(int id, string title, string description, string name, float BulletDamage, float BulletSpeed, float ReloadSpeed) : base(id,ItemTypes.HOTWEAPON,title,description, name)
    {
        this.BulletDamage = BulletDamage;
        this.BulletSpeed = BulletSpeed;
        this.ReloadSpeed = ReloadSpeed;
        this.prefab = Resources.Load<GameObject>("Prefabs/Weapons/Hot/" + name);
    }
    public HotWeapon() : base()
    {

    }
}
