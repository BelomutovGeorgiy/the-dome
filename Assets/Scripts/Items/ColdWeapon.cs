﻿using UnityEngine;
using System.Collections;

public class ColdWeapon : Item {

	public float Damage { get; set; }
    public float CritChanse { get; set; }///*from 0 to 1*///

    public GameObject prefab;

    public ColdWeapon(int id, string title, string description, string name, float damage, float crit) : base(id,ItemTypes.COLDWEAPON,title,description,name)
    {
        this.Damage = damage;
        this.CritChanse = crit;
        this.prefab = Resources.Load<GameObject>("Prefabs/Weapons/Cold/" + name);
    }
    public ColdWeapon() : base()
    {

    }
}
