﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ArmorSlot : EquipmentSlot {

    public ArmorTypes armorType;

    public override void OnDrop(PointerEventData eventData)
    {
        ItemData droppedItem = eventData.pointerDrag.GetComponent<ItemData>();
        if (droppedItem.item.Type == type)
            if((droppedItem.item as Armor).Atype==armorType)
        {
            if (inventory.items[id].ID == -1)
            {

                inventory.items[droppedItem.slot] = new Item();
                inventory.items[id] = droppedItem.item;
                droppedItem.slot = id;

                pickItem(droppedItem);


            }
            else
            {
                Transform item = this.transform.GetChild(1);
                item.GetComponent<ItemData>().slot = droppedItem.slot;
                item.transform.SetParent(inventory.slots[droppedItem.slot].transform);
                item.transform.position = inventory.slots[droppedItem.slot].transform.position;

                droppedItem.slot = id;
                droppedItem.transform.position = this.transform.position;

                pickItem(droppedItem);


                inventory.items[droppedItem.slot] = item.GetComponent<ItemData>().item;
                inventory.items[id] = droppedItem.item;
            }
        }
    }
}
