﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.Collections.Generic;
using System.IO;
using System;

public class ItemsBase : MonoBehaviour
{

    public List<Item> itemsBase = new List<Item>();
    private JsonData itemData;
    // Use this for initialization
    void Awake()
    {
        itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + Constants._ITEMSBASEPATH));
        BuildBase();
    }
    void BuildBase()
    {
        for(int i=0;i<itemData.Count;i++)
        {
            ItemTypes type = (ItemTypes)Enum.Parse(typeof(ItemTypes), itemData[i]["type"].ToString());
            switch (type)
            {
                case ItemTypes.COLDWEAPON:
                    itemsBase.Add(new ColdWeapon((int)itemData[i]["id"], itemData[i]["title"].ToString(), itemData[i]["description"].ToString(), itemData[i]["name"].ToString(), float.Parse(itemData[i]["damage"].ToString()), float.Parse(itemData[i]["critChanse"].ToString())));
                    break;
                case ItemTypes.HOTWEAPON:
                    itemsBase.Add(new HotWeapon((int)itemData[i]["id"], itemData[i]["title"].ToString(), itemData[i]["description"].ToString(), itemData[i]["name"].ToString(), float.Parse(itemData[i]["bulletDamage"].ToString()), float.Parse(itemData[i]["bulletSpeed"].ToString()), float.Parse(itemData[i]["reloadSpeed"].ToString())));
                    break;
                case ItemTypes.ARMOR:
                    itemsBase.Add(new Armor((int)itemData[i]["id"], itemData[i]["title"].ToString(), itemData[i]["description"].ToString(), itemData[i]["name"].ToString(), float.Parse(itemData[i]["armor"].ToString())));
                    break;
                default:
                    itemsBase.Add(new Item((int)itemData[i]["id"], type, itemData[i]["title"].ToString(), itemData[i]["description"].ToString(), itemData[i]["name"].ToString()));
                    break;
            }
        }
    }

    public Item FindItemByID(int id)
    {
        foreach (Item i in itemsBase)
        {
            if (i.ID == id)
                return i;
        }
        return null;
    }
}
