﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Item
{

    public int ID { get; set; }
    public ItemTypes Type { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Name { get; set; }
    public Sprite Sprite { get; set; }


    public Item(int id, ItemTypes type, string title, string description, string name)
    {
        this.Type = type;
        this.ID = id;
        this.Title = title;
        this.Description = description;
        this.Name = name;
        this.Sprite = Resources.Load<Sprite>("Sprites/Items/"+name);
    }

    public Item()
    {
        this.ID = -1;
    }

}

