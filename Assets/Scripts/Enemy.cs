﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class Enemy : Character {
    
    [Serializable]
    public struct DropIDChanse
    {
        public int ID;
        [Range(0.01f, 1)]
        public float Chanse;
    }

    public DropIDChanse[] Drop;
    public GameObject target;

    public float searchDistance;

	public Slider HPBarObject=null;

    public Transform damager;
    public float damagerRadius;
    
	// Update is called once per frame
	public virtual void LateUpdate () {
		BarAssigned ();
	}

    protected Slider clone=null;
    public void BarAssigned()
	{
        
		if((HP!=HPMax) && (clone==null))
		{
            clone =Instantiate(HPBarObject) as Slider;
            clone.GetComponent<HPBar>().Owner = this.gameObject;
        }
	}
    public override void DestroyObject()
    {
        foreach (DropIDChanse item in Drop)
        {
            float rnd = UnityEngine.Random.Range(0.0f, 1.0f);
            if (rnd <= item.Chanse)
                GiveItem(item.ID);
        }
        Destroy(gameObject);
    }
    public void GiveItem(int itemID)
    {
        DialoguePanel panel = Constants.Manager.GetComponent<Refery>().DialoguePanel.GetComponent<DialoguePanel>();
        Constants.Manager.GetComponent<Refery>().Player.GetComponent<Inventory>().AddItem(itemID);
        
       
        if(panel.Buttons.Count==0)
        {
            panel.UpdateAsk("You got: " + Constants.Manager.GetComponent<ItemsBase>().FindItemByID(itemID).Title);
            panel.SetContinueButton();
            panel.Buttons[0].GetComponent<Button>().onClick.AddListener(panel.Hide);
            panel.Show();
        }
        else
        {
            panel.UpdateAsk(panel.Text+", "+ Constants.Manager.GetComponent<ItemsBase>().FindItemByID(itemID).Title);
        }
        
        
    }
}

