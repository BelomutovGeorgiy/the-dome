﻿using UnityEngine;
using System.Collections;

public class Converter : MonoBehaviour {

    private Animator anim;

    public GameObject outputobj;
    private GameObject incomobj;
    void Awake()
    {
        anim = GetComponent<Animator>();
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.GetComponent<Character>())
        {
            anim.SetTrigger("income");
            incomobj = coll.gameObject;
        }
    }
    public void convert()
    {
        
       GameObject clone= Instantiate(outputobj) as GameObject;
        
        clone.layer = incomobj.layer;
        clone.transform.SetParent(incomobj.transform.parent);
        Destroy(incomobj);
       
        clone.transform.localPosition = incomobj.transform.localPosition;
        clone.transform.localScale = incomobj.transform.localScale;
        incomobj = null;
    }
}
