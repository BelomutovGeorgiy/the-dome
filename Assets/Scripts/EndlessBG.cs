﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndlessBG : MonoBehaviour {

    public Transform player;
    private int direction;
    private Vector3 backPos;
    public float width = 0.0f;
    public float height = +38.36f;
    private float X;
    private float Y;
    public float Z = 30;
    void OnBecameInvisible()//когда объект становится невидимиым
    {
        //просчитываю текущую позицию объекта
        backPos = gameObject.transform.position;
        if (player.transform.position.y > backPos.y)//проверка, в какую сторону перерисовываться облакам 
        {
            direction = -1;
        }
        else
        {
            direction = 1;
        }
        //просчитываю новую позицию
        //print (backPos);//вывожу в консоль текущую позицию(это больше не нужно)
        X = backPos.x + width;
        Y = backPos.y - height * direction;
        //двигаю объект на новую позицию
        gameObject.transform.position = new Vector3(X, Y, Z);
    }
}




