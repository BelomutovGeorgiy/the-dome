﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Refery : MonoBehaviour {

    public GameObject DialoguePanel;
    public GameObject Player;
    public Image DieImage;

    private Color DieImageColorFade;
    private Color DieImagecolor;

    public float speed = 10;
    // Use this for initialization
    void Awake () {
        DieImageColorFade = DieImage.color;
        DieImageColorFade.a = 0;
        DieImagecolor = DieImage.color;
        DieImagecolor.a = 1;
    }
	
	// Update is called once per frame
	void Update () {

        if (!Player)
        {
            //yield return new WaitForSeconds(5f);
            
            StartCoroutine(showTextFuntion());
        }
        else
            DieImage.color = Color.Lerp(DieImage.color, DieImageColorFade, speed * Time.deltaTime);
    }
    IEnumerator showTextFuntion()
    {
        yield return new WaitForSeconds(3f);
        DieImage.color = Color.Lerp(DieImage.color, DieImagecolor, speed * Time.deltaTime);
        yield return new WaitForSeconds(7f);

        Debug.Log("restart");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

}
