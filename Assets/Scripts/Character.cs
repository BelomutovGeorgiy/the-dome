﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Character : MonoBehaviour  {

	public float HPMax=100;
    public float StaminaMin = 25;
	public float StaminaMax=100;
	public float Armor=0;
	public float Damage=10;
	public float AttackCost=5;
    public float StaminaRegenSpeed = 4f;

    [HideInInspector]
    protected float RealDamage;
    [HideInInspector]
    public float HP;
 //   [HideInInspector]
    public float Stamina;


	public bool isFacingRight = true;

    [HideInInspector]
    public Rigidbody2D rb;
    [HideInInspector]
    public Animator anim;
    [HideInInspector]
	public AudioSource asource;


	public AudioClip hitSound;

    public AudioClip[] stepSounds;


	public float HPCoolDown=3;
	private float hpstamp;
    private float prevhp;

    public void Awake()
    {
        rb = this.gameObject.GetComponent<Rigidbody2D>();
        asource = this.gameObject.GetComponent<AudioSource>();
        anim = this.gameObject.GetComponent<Animator>();
    }

	public virtual void Start () {
		HP = HPMax;
        prevhp = HPMax;
		Stamina = StaminaMax;
	}

	public virtual void Update () {
        if (HP <= 0)
        {
            if (GetComponent(typeof(ICharControl)))
                 Destroy(GetComponent(typeof(ICharControl)));
            anim.SetTrigger("destroy");
        }
	}


	public void Flip()
	{
		//меняем направление движения персонажа
		isFacingRight = !isFacingRight;
		//получаем размеры персонажа
		Vector3 theScale = transform.localScale;
		//зеркально отражаем персонажа по оси Х
		theScale.x *= -1;
		//задаем новый размер персонажа, равный старому, но зеркально отраженный
		transform.localScale = theScale;
    }

    public void Jump(float force)
    {
        rb.AddForce(new Vector2(0, force));
    }
    public void Move(Vector3 distance)
    {
        transform.Translate(distance);
    }

    public void StepSoundPlay()
    {
        asource.clip = stepSounds[Random.Range(0, stepSounds.Length - 1)];
        asource.Play();
    }

    public virtual void DestroyObject()
    {
        Destroy(gameObject);
    }
}
