﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerStamina : MonoBehaviour {

    public Slider slider;
    // Use this for initialization
    public GameObject Owner = null;
    private Player player = null;

    public void Awake()
    {
        player = Owner.GetComponent<Player>();
        slider.maxValue = player.StaminaMax/100;
        slider.value = player.StaminaMax/100;
    }

    public void Update () {
      //  Debug.Log(player.Stamina);
        slider.value = player.Stamina/100;
	}

}
