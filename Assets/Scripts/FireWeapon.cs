﻿using UnityEngine;
using System.Collections;

public class FireWeapon : Weapon {

    public float bulletSpeed = 10; // скорость пули
    public float fireRate = 1; // скорострельность

    public Transform LeftIK;
    public Transform RightIK;

    public GameObject shooter;
    public GameObject dir;
    public GameObject bullet;

    public GameObject MuzzleFlashPrefab;

    private float curTimeout;

    private PlayerControl pController;


    // Use this for initialization
    void Start () {
        pController = GameObject.Find("Manager").GetComponent<Refery>().Player.GetComponent<PlayerControl>();

    }
	
	// Update is called once per frame
	void Update () {
        curTimeout += Time.deltaTime;
        if (fireRate == 0)
        {
            if (Input.GetButtonDown("Fire1") && !pController.isCrawling)
            {
                Fire();
            }
        }
        else
            if (Input.GetButton("Fire1") && Time.time > curTimeout && !pController.isCrawling)
        {
            if (curTimeout > fireRate)
            {
              //  curTimeout = Time.time + 1 / fireRate;
                Fire();
            }
        }
    }

    public void Fire()
    {
        Effect();

        
            curTimeout = 0;
        /* GameObject clone=null;
         Vector2 velocity =Vector2.zero;
         if (transform.parent.parent.parent.GetComponent<Player>().isFacingRight)
         {
             clone = Instantiate(bullet, shooter.transform.position, shooter.transform.rotation) as GameObject;
             velocity = shooter.transform.right*bulletSpeed;
         }
         else
         {
             clone = Instantiate(bullet, shooter.transform.position,Quaternion.identity) as GameObject;
             velocity = shooter.transform.right * bulletSpeed*(-1);
         }*/

        Vector3 direction = shooter.transform.position - dir.transform.position;
        direction.Normalize();
        GameObject clone = Instantiate(bullet, shooter.transform.position, Quaternion.identity) as GameObject;
        clone.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;
        clone.transform.right = direction;


        // clone.transform.localScale = shooter.transform.lossyScale;
        //clone.GetComponent<ShotScript>().damage = damage;
        //clone.GetComponent<Rigidbody2D>().velocity =Mathf.Sign(shooter.transform.lossyScale.x)*bulletSpeed*shooter.transform.right;
        //clone.GetComponent<Rigidbody2D>().velocity =velocity;


    }

    void Effect()
    {
        iTween.ShakePosition(Camera.main.gameObject, new Vector3(0.2f, 0.2f, 0.02f), 0.2f);
        Hashtable gsparams = new Hashtable();
        gsparams.Add("amount", new Vector3(0.2f, 0.2f, 0));
        gsparams.Add("islocal", true);
        gsparams.Add("time", 0.02f);
        iTween.ShakePosition(gameObject,gsparams);
        GameObject muzzle = Instantiate(MuzzleFlashPrefab, shooter.transform.position,Quaternion.identity) as GameObject;

        Vector3 direction = shooter.transform.position - dir.transform.position;
        direction.Normalize();


        float scale = Random.Range(0.6f, 0.9f);
        
        muzzle.transform.localScale*=scale;

        

        muzzle.transform.right = direction;

       // muzzle.transform.SetParent(shooter.transform);
        Destroy(muzzle, 0.02f);
    }
}
