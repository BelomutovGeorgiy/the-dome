﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System;

[ExecuteInEditMode]
public class Bone : MonoBehaviour
{
    private Bone parent;

#if UNITY_EDITOR
    [MenuItem("Alone Programmer Works/Create/Bone")]
    public static Bone Create()
    {
        GameObject b = new GameObject("Bone");
        Undo.RegisterCreatedObjectUndo(b, "Add child bone");
        b.AddComponent<Bone>();

        if (Selection.activeGameObject != null)
        {
            
            GameObject sel = Selection.activeGameObject;
            b.transform.parent = sel.transform;
            
            if (sel.GetComponent<Bone>() != null)
            {
                
                Bone p = sel.GetComponent<Bone>();
                b.transform.localRotation = Quaternion.Euler(0, 0, 0);
                b.transform.position = b.transform.parent.position + new Vector3(1,1,0);
                // SetIcon(b);
                Debug.Log(sel.GetComponent<Bone>());
            }
            else
            {
                b.name = "Root";
            }

        }
        else
        {
            b.name = "Root";
        }

        Selection.activeGameObject = b;

        return b.GetComponent<Bone>();
    }
#endif

    // Use this for initialization
    void Start()
    {
        if (gameObject.transform.parent != null)
            parent = gameObject.transform.parent.GetComponent<Bone>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.localRotation = Quaternion.Euler(0, 0, transform.localRotation.eulerAngles.z);

/*#if UNITY_EDITOR
        if (Application.isEditor && editMode && snapToParent && parent != null)
        {
            gameObject.transform.position = parent.Head;
        }
#endif*/
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if(transform.parent)
        if (transform.parent.GetComponent<Bone>())
        {
                Gizmos.color = Color.cyan;
                if (gameObject == Selection.activeGameObject)
                {
                    Gizmos.color = Color.yellow;
                }
                // Gizmos.DrawLine(transform.position, transform.parent.position);
                Gizmos.DrawWireSphere(transform.position, 0.03f);


                /*Vector3 v = Quaternion.AngleAxis(-45, Vector3.forward) * ((transform.parent.position - gameObject.transform.position) / Vector3.Distance(transform.position,transform.parent.position));
                Gizmos.DrawLine(gameObject.transform.position, gameObject.transform.position + v);
                Gizmos.DrawLine(gameObject.transform.position + v, transform.parent.position);*/

                Vector3 v = Quaternion.AngleAxis(-135, Vector3.forward) * ((transform.parent.position - gameObject.transform.position));
                Gizmos.DrawLine(gameObject.transform.parent.position, gameObject.transform.parent.position + v/8);
                Gizmos.DrawLine(gameObject.transform.parent.position + v/8, transform.position);

                v = Quaternion.AngleAxis(135, Vector3.forward) * ((transform.parent.position - gameObject.transform.position));
                Gizmos.DrawLine(gameObject.transform.parent.position, gameObject.transform.parent.position + v/8);
                Gizmos.DrawLine(gameObject.transform.parent.position + v/8, transform.position);

                Gizmos.DrawLine(gameObject.transform.position, transform.parent.position);
            }
        
        /*    Vector3 v = Quaternion.AngleAxis(45, Vector3.forward) * (((Vector3)Head - gameObject.transform.position) / div);
        Gizmos.DrawLine(gameObject.transform.position, gameObject.transform.position + v);
        Gizmos.DrawLine(gameObject.transform.position + v, Head);

        v = Quaternion.AngleAxis(-45, Vector3.forward) * (((Vector3)Head - gameObject.transform.position) / div);
        Gizmos.DrawLine(gameObject.transform.position, gameObject.transform.position + v);
        Gizmos.DrawLine(gameObject.transform.position + v, Head);

        Gizmos.DrawLine(gameObject.transform.position, Head);

        Gizmos.color = new Color(Gizmos.color.r, Gizmos.color.g, Gizmos.color.b, 0.5f);

        if (deform && editMode && showInfluence)
        {
            Gizmos.DrawWireSphere(transform.position, influenceTail);
            Gizmos.DrawWireSphere(Head, influenceHead);
        }*/
    }
#endif
   /* private static GUIContent[] largeIcons;

    public static void SetIcon(GameObject gObj)
    {
        if (largeIcons == null)
        {
            largeIcons = GetTextures("sv_icon_dot", "_pix16_gizmo", 0, 16);
        }
        SetIcon(gObj, largeIcons[9].image as Texture2D);
    }
    private static void SetIcon(GameObject gObj, Texture2D texture)
    {
        var ty = typeof(EditorGUIUtility);
        var mi = ty.GetMethod("SetIconForObject", BindingFlags.NonPublic | BindingFlags.Static);
        mi.Invoke(null, new object[] { gObj, texture });
    }

    private static GUIContent[] GetTextures(string baseName, string postFix, int startIndex, int count)
    {
        GUIContent[] guiContentArray = new GUIContent[count];

        var t = typeof(EditorGUIUtility);
        var mi = t.GetMethod("IconContent", BindingFlags.NonPublic | BindingFlags.Static, null, new Type[] { typeof(string) }, null);

        for (int index = 0; index < count; ++index)
        {
            guiContentArray[index] = mi.Invoke(null, new object[] { baseName + (object)(startIndex + index) + postFix }) as GUIContent;
        }

        return guiContentArray;
    }*/

    /*  public float GetInfluence(Vector2 p)
      {
          Vector2 wv = Head - (Vector2)transform.position;

          float dist = 0;

          if (length == 0)
          {
              dist = (p - (Vector2)transform.position).magnitude;
              if (dist > influenceTail)
                  return 0;
              else
                  return influenceTail;
          }
          else
          {
              float t = Vector2.Dot(p - (Vector2)transform.position, wv) / wv.sqrMagnitude;

              if (t < 0)
              {
                  dist = (p - (Vector2)transform.position).magnitude;
                  if (dist > influenceTail)
                      return 0;
                  else
                      return (influenceTail - dist) / influenceTail;
              }
              else if (t > 1.0f)
              {
                  dist = (p - Head).magnitude;
                  if (dist > influenceHead)
                      return 0;
                  else
                      return (influenceHead - dist) / influenceHead;
              }
              else
              {
                  Vector2 proj = (Vector2)transform.position + (wv * t);
                  dist = (proj - p).magnitude;

                  float s = (influenceHead - influenceTail);
                  float i = influenceTail + s * t;

                  if (dist > i)
                      return 0;
                  else
                      return (i - dist) / i;
              }
          }
      }

      internal int GetMaxIndex()
      {
          Bone[] bones = transform.root.GetComponentsInChildren<Bone>();

          if (bones == null || bones.Length == 0)
              return 0;

          return bones.Max(b => b.index) + 1;
      }*/
}
