﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerControl : MonoBehaviour, ICharControl
{
    const float height = 3.50f;

    public GameObject plr;

    public float walkSpeed = 3.0f;
    public float jumpSpeed = 5.0f;
    public float boost = 1f;

    private Player player;

    void Awake()
    {
        player = plr.GetComponent<Player>();
    }
    // Use this for initialization
    void Start()
    {

    }


    float h;
    float v;
    bool isRun;
    bool isJump;
    bool isAttack;
    bool isInteraction;
    bool isInventory;
    bool ground;
   
   public bool isCrawling = false;

    // Update is called once per frame
    void Update()
    {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");
        ground = player.isOnGround();

        isRun = Input.GetButton("Run");
        isJump = Input.GetButtonDown("Jump");
        isAttack = Input.GetButtonDown("Attack");
        isInteraction = Input.GetButtonDown("Interaction");
        isInventory = Input.GetButtonDown("Inventory");

        if (isRun && !isCrawling)//если включен спринт, ось домножается на boost
        {
            if (!player.staminaLock)
            {
                player.Stamina -= 4;
                h *= boost;
            }
        }
        if (isInteraction)
        {
            player.Interaction();
        }
        if (ground && !isCrawling)
        {
            if (isJump)
            {
                player.Jump(jumpSpeed);
            }
            if (isAttack && !player.staminaLock)
            {
                player.anim.SetTrigger("Attack");
            }
        }
        if (isInventory)
        {
            GetComponent<Inventory>().inventoryPanel.transform.parent.gameObject.SetActive(!GetComponent<Inventory>().inventoryPanel.transform.parent.gameObject.activeInHierarchy);
        }
        if (player.isOnLadder())
        {

            isCrawling = false;
            player.anim.SetBool("crawling", isCrawling);
            if (v != 0)
            {
                player.anim.SetBool("laddered", true);
                player.rb.velocity = Vector2.zero;
                player.rb.isKinematic = true;
            }
            player.Move(new Vector3(0, v * Time.deltaTime * walkSpeed, 0));
        }
        else
        {
            if (v < 0 && !isCrawling)
            {
                isCrawling = true;
                player.anim.SetBool("crawling", isCrawling);
            }
            if (player.rb.isKinematic)
            {
                player.rb.isKinematic = false;
                player.anim.SetBool("laddered", false);
            }
        }
        if (v >= 0 && isCrawling && isSpaceEnough())
        {
            isCrawling = false;
            player.anim.SetBool("crawling", isCrawling);
        }



        player.Move(new Vector3(h * Time.deltaTime * walkSpeed, 0, 0));




        if (h > 0 && !player.isFacingRight)
            player.Flip();
        else if (h < 0 && player.isFacingRight)
            player.Flip();


        player.anim.SetFloat("speed", Math.Abs(h));
        player.anim.SetBool("grounded", ground);
        player.anim.SetFloat("vspeed", player.rb.velocity.y);

    }
    bool isSpaceEnough()
    {
        Vector3 dir = Vector3.up * height;
        Vector3 point = player.groundCheck.position + new Vector3(0, 0.5f, 0);
        Debug.DrawRay(point, dir, Color.red);

        //ИЗЗЗЗЯЩНО!!!

        RaycastHit2D h = Physics2D.Raycast(point, dir,height, (1<<0)|(1<<8));//кастую луч по высоте перса с маской слоев умолчания и земли

        if (h.transform)
            if (h.transform.GetComponent<SpriteRenderer>())
                h.transform.GetComponent<SpriteRenderer>().color = Color.cyan;// ЧИСТО ДЕБАГ  

        //И если есть коллайдер и не триггер
        if (h.collider && !h.collider.isTrigger)
        {
            return false;
        }
        return true;


        //Debug.Log(hit.Length);
        // 
        /*foreach (RaycastHit2D h in hit)
        {*/


      

        // Debug.Log(h.collider.gameObject.layer);

        //}
    }
    //float external = 0;
    void FixedUpdate()
    {

        /* if (h * GetComponent<Rigidbody2D>().velocity.x < player.walkSpeed)
             // ... add a force to the player.
             GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * player.accel);

         // If the player's horizontal velocity is greater than the maxSpeed...
         if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > player.walkSpeed)
             // ... set the player's velocity to the maxSpeed in the x axis.
             GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x) * player.walkSpeed, GetComponent<Rigidbody2D>().velocity.y);*/


        //player.rb.velocity.x-(player.walkSpeed*h) -- external velocity
        // If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
        /* if (Mathf.Abs(player.rb.velocity.x*h) < player.walkSpeed * Mathf.Abs(h))
             player.rb.AddForce(new Vector2(h*player.accel,0), ForceMode2D.Force);
         if (Mathf.Abs(player.rb.velocity.x * h) > player.walkSpeed * Mathf.Abs(h))
             player.rb.velocity = new Vector2(player.walkSpeed*h+external,player.rb.velocity.y);

         player.rb.
         // if(Mathf.Abs(h)<1)
         external = h * player.walkSpeed- player.rb.velocity.x;*/
        // player.rb.velocity=( new Vector2(player.walkSpeed*h ,player.rb.velocity.y));
        // player.rb.velocity=(new Vector2(prevXVelocity + player.rb.velocity.x - (player.walkSpeed * h), player.rb.velocity.y));

        /*else
            player.rb.velocity = new Vector2(player.rb.velocity.x, player.rb.velocity.y);*/
        // If the player's horizontal velocity is greater than the maxSpeed...
        /* if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > player.walkSpeed)
             // ... set the player's velocity to the maxSpeed in the x axis.
             GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x) * player.walkSpeed, GetComponent<Rigidbody2D>().velocity.y);*/
        //print(player.rb.velocity.x - (player.walkSpeed * h));
    }
}
