﻿using UnityEngine;
using System.Collections;

public class Prisoner_Runner : MonoBehaviour {

    public GameObject Boss;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Player")
        {
            Boss.GetComponent<Animator>().SetTrigger("begin");
            Destroy(this);
        }
    }
}
