﻿using UnityEngine;
using System.Collections;

public class TriggerEvents : MonoBehaviour {

    public AudioSource source;

    public AudioClip clipForChange;

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (clipForChange)
        {
            source.Stop();
            source.clip = clipForChange;
            source.Play();
        }
    }
}
