﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tooltip : MonoBehaviour
{

    public static string text;
    public static bool isUI;

    public Color BGColor = Color.white;
    public Color textColor = Color.black;

    public int fontSize = 14; // размер шрифта

    public Text boxText;
    public Camera _camera;
    public float speed = 10; // скорость плавного затухания и проявления


    private Color textColorFade;

    void Awake()
    {
        textColorFade = textColor;
        textColorFade.a = 0;
        isUI = false;



        boxText.color = textColorFade;
    }

    private bool show = false;
    void LateUpdate()
    {
        
        boxText.fontSize = fontSize;
        boxText.text = text;


        if (show || isUI)
        {      
            boxText.color = Color.Lerp(boxText.color, textColor, speed * Time.deltaTime);
        }
        else
        {
            boxText.color = Color.Lerp(boxText.color, textColorFade, speed * Time.deltaTime);
        }
    }
    public void Show(string message)
    {
        show = true;
        text = message;
    }
    public void Hide()
    {
        show = false;
    }
}