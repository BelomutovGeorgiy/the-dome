﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ThePrisonerAI : Boss {

    // Use this for initialization

    // Update is called once per frame

    private Vector3 m_LastTargetPosition;
    private Vector3 m_LookAheadPos;
    private float m_OffsetZ;
    private Vector3 m_CurrentVelocity = Vector3.zero;

    public float lookAheadMoveThreshold = 0.1f;
    public float lookAheadFactor = 3;
    public float lookAheadReturnSpeed = 0.5f;
    public float damping = 0.3f;

    public Transform[] positions;
    public Transform Root;
    public Transform IK;

    public Transform[] Fists;
    public Transform[] FistsIK;
    private Vector3[] FistsStartPosition;
    private float[] TargetYPositions;
    private float[] RestTime;

    public Vector3 BossPosition;
    public float MoveDelay;
    public LayerMask targetLayer;

    private float MoveCurTime = 0.0f;
    // Use this for initialization


    // Update is called once per frame
    public override void Start()
    {
        StageList = new List<BossStage>();
        StageList.Add(Stage0);
        StageList.Add(Stage2);

        HP = HPMax;
        Stamina = StaminaMax;
        target = GameObject.Find("Manager").GetComponent<Refery>().Player;
    }
    public void StartBossEvent()
    {
        GetComponent<Animator>().enabled = false;
       // GetComponent<Animator>().SetTrigger("freezeAnim");

        FistsStartPosition = new Vector3[Fists.Length];
        TargetYPositions = new float[Fists.Length];
        RestTime = new float[Fists.Length];

        BarAssigned();

        for (int i = 0; i < Fists.Length; i++)
        {
            FistsStartPosition[i] = FistsIK[i].position;
            RestTime[i] = 0;
        }
        currentStage++;
        Camera.main.GetComponent<AudioSource>().clip = BossMusicThemes[currentStage];
        Camera.main.GetComponent<AudioSource>().Play();
    }
    public void Stage0()
    {

    }
    public void Stage2()
    {
        if (HP <= 0)
        {
            GetComponent<Animator>().enabled =true;
            GetComponent<Animator>().SetTrigger("kill");
            currentStage--;
        }

        bool isAllRestEqualZero = true;
        foreach (float f in RestTime)
        {
            if (f > 0)
                isAllRestEqualZero = false;
        }

        if (MoveCurTime >= MoveDelay)
        {
            MoveCurTime = 0;
            if (isAllRestEqualZero)
            {
                Vector3 TempPos = positions[Random.Range(0, positions.Length - 1)].position;
                BossPosition = new Vector3(TempPos.x, Root.position.y, Root.position.z);
            }
        }
        else
        {
            MoveCurTime += Time.deltaTime;
        }

        int fistIndex = isUnderFist();
        if (fistIndex != -1)
        {
            Attack(fistIndex);
        }

        for (int i = 0; i < Fists.Length; i++)
        {
            if (RestTime[i] <= 0)
            {
                TargetYPositions[i] = FistsStartPosition[i].y;
            }
            else
            {
                RestTime[i] -= Time.deltaTime;
            }
            Vector3 calculatedPos = new Vector3(FistsIK[i].position.x, TargetYPositions[i], FistsIK[i].position.z);
            FistsIK[i].transform.position = Vector3.Lerp(FistsIK[i].transform.position,
                                                                    calculatedPos,
                                                                     Time.deltaTime * damping * 7);
        }
        float step = Time.deltaTime * damping;
        float ikstep = step / 2f;

        Root.transform.position = Vector3.Lerp(Root.transform.position, BossPosition, step);
        IK.transform.position = Vector3.Lerp(IK.transform.position, BossPosition, ikstep);
    }

    private void Attack(int fistIndex)
    {
        if (RestTime[fistIndex] <= 0)
        {
            Character chr=target.GetComponent<Character>();
            chr.asource.clip = chr.hitSound;
            chr.asource.Play();
            chr.anim.SetTrigger("hit");
            chr.HP -= Damage;
            TargetYPositions[fistIndex] = positions[0].position.y;
            RestTime[fistIndex] = 1;
        }
    }
    private int isUnderFist()
    {
        Vector3 dir = new Vector3(0, -1) * 7;
        for (int i = 0; i < Fists.Length; i++)
        {

            Debug.DrawRay(Fists[i].position, dir, Color.blue);
            RaycastHit2D hit = Physics2D.Raycast(Fists[i].position, dir, 7, targetLayer);
            if (hit.collider && hit.transform.tag == "Player")
            {
                return i;
            }
        }
        return -1;
    }

    public override void LateUpdate()
    {

    }

}
