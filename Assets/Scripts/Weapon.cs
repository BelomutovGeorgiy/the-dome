﻿using UnityEngine;
using System.Collections;
using System;

public class Weapon : MonoBehaviour {

    public Transform damager;
    public float damage;
    public float radius;
    public float weight=200;
    public bool isFireWeapon = false;


    public void attack(int EnemyLayer)
    {
        Fight2D.Action(damager.position, radius, EnemyLayer, damage, false);
    }


}
