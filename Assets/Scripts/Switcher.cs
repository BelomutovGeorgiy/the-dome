﻿using UnityEngine;
using System.Collections;

public class Switcher : Swithable {

    public GameObject[] objects;

    // Use this for initialization


    public override void SwitchOn()
    {
        GetComponent<Animation>().Play("on");
        foreach (GameObject go in objects)
            go.GetComponent<Swithable>().Switch();
    }
    public override void SwitchOff()
    {
        GetComponent<Animation>().Play("off");
        foreach (GameObject go in objects)
            go.GetComponent<Swithable>().Switch();
    }
}
