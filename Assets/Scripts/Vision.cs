﻿using UnityEngine;
using System.Collections;
using System;

public class Vision : MonoBehaviour
{

    public Material defMat;
    public Material selMat;
    public string[] whatWeSee;
    //public GameObject player;
    private float radius;
    public Canvas Tolltips;
    // Use this for initialization

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (Array.BinarySearch(whatWeSee, coll.tag) > -1)
        {
            if (coll.GetComponent<SpriteRenderer>())
               coll.gameObject.GetComponent<SpriteRenderer>().material = selMat;
            foreach (Transform child in coll.transform)
            {
                if (child.GetComponent<SpriteRenderer>())
                    child.GetComponent<SpriteRenderer>().material = selMat;
            }
            
        }
        if (coll.GetComponent<ToolTipText>())
            Tolltips.GetComponent<Tooltip>().Show(coll.GetComponent<ToolTipText>().text);
    }
    void OnTriggerExit2D(Collider2D coll)
    {
        if (Array.BinarySearch(whatWeSee, coll.tag) > -1)
        {
            if (coll.GetComponent<SpriteRenderer>())
                coll.gameObject.GetComponent<SpriteRenderer>().material = defMat;
            foreach (Transform child in coll.transform)
            {
                if (child.GetComponent<SpriteRenderer>())
                    child.GetComponent<SpriteRenderer>().material = defMat;
            }
            
        }
        if (coll.GetComponent<ToolTipText>())
            Tolltips.GetComponent<Tooltip>().Hide();
        if (coll.GetComponent<Dialogue>())
            coll.GetComponent<Dialogue>().StopParse();
    }
}
