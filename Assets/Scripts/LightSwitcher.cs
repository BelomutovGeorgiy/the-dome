﻿using UnityEngine;
using System.Collections;
using System;

public class LightSwitcher : Swithable {

    public Light[] lights;

    public override void SwitchOn()
    {
        foreach (Light l in lights)
            l.intensity = 1;
    }

    public override void SwitchOff()
    {
        foreach (Light l in lights)
            l.intensity = 0;
    }
}
