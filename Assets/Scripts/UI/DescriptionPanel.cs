﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DescriptionPanel : MonoBehaviour {


    public Text title;
    public Text description;

    public Image preview;

    private CanvasGroup cg;

    public void Awake()
    {
        cg = GetComponent<CanvasGroup>();
    }
   public void Hide()
    {
        //iTween.FadeTo(gameObject, 0, 0.5f);
        //gameObject.SetActive(false);
        cg.alpha = 0;
    }

   public void Show(ItemData item)
    {
        title.text = item.item.Title;
        description.text = item.item.Description;
        preview.sprite = item.item.Sprite;
        cg.alpha = 1;
        // gameObject.SetActive(true);
        // iTween.FadeTo(gameObject, 1, 0.5f);
    }
}
