﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Boss : Enemy, ICharControl
{
    public delegate void BossStage();

    protected int currentStage = 0;
    protected List<BossStage> StageList;

    public List<AudioClip> BossMusicThemes;

    public override void Update()
    {
        StageList[currentStage]();
    }


    public override void DestroyObject()
    {   
        
        foreach (DropIDChanse item in Drop)
        {
            float rnd = Random.Range(0.0f, 1.0f);
            if (rnd<=item.Chanse)
            GiveItem(item.ID);
        }
        Camera.main.GetComponent<AudioSource>().Stop();
        Destroy(gameObject);
    }
    public new void BarAssigned()
    {
            clone = Instantiate(HPBarObject) as Slider;
            clone.GetComponent<HPBar>().Owner = this.gameObject;
    }
}
