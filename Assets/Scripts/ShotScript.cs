﻿using UnityEngine;
using System.Collections;

public class ShotScript : MonoBehaviour {
    public float damage;

    void Start()
    {
        Destroy(gameObject, 20);
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.GetComponent<Character>())
        {
            coll.gameObject.GetComponent<Character>().HP -= damage;
        }
        if (!coll.isTrigger)
            Destroy(gameObject);
    }
}
