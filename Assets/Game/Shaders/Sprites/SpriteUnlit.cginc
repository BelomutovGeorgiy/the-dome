#ifndef SPRITE_UNLIT_INCLUDED
#define SPRITE_UNLIT_INCLUDED

#include "ShaderShared.cginc"

////////////////////////////////////////
// Vertex structs
//
				
struct vertexInput
{
	float4 vertex : POSITION;
	float4 texcoord : TEXCOORD0;
	fixed4 color : COLOR;
};

struct vertexOutput
{
	float4 pos : SV_POSITION;
	float2 texcoord : TEXCOORD0;
	fixed4 color : COLOR;
#if defined(_FOG)
	UNITY_FOG_COORDS(1)
#endif // _FOG	
};

////////////////////////////////////////
// Vertex program
//

vertexOutput vert(vertexInput input)
{
	vertexOutput output;
	
	output.pos = calculateLocalPos(input.vertex);	
	output.texcoord = calculateTextureCoord(input.texcoord);
	output.color = calculateVertexColor(input.color);

#if defined(_FOG)
	UNITY_TRANSFER_FOG(output,output.pos);
#endif // _FOG	
	
	return output;
}

////////////////////////////////////////
// Fragment program
//

fixed4 frag(vertexOutput input) : SV_Target
{
	fixed4 texureColor = calculateTexturePixel(input.texcoord.xy);
	ALPHA_CLIP(texureColor.a, input.color)

	fixed4 pixel = calculatePixel(texureColor, input.color);
	
	COLORISE(pixel)
	
#if defined(_FOG)
	UNITY_APPLY_FOG(input.fogCoord, pixel.rgb);
#endif // _FOG	
	
	return pixel;
}

#endif // SPRITE_UNLIT_INCLUDED